PRODUCT_DEVICE := rpi4
PRODUCT_BRAND := raspberrypi
PRODUCT_MODEL := Raspberry Pi 4 Model B
PRODUCT_MANUFACTURER := Raspberry Pi Foundation
PRODUCT_RELEASE_NAME := Raspberry Pi 4
