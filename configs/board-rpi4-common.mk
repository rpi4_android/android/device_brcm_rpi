#
# Copyright (C) 2017 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Save time building, switch later
DONT_DEXPREOPT_PREBUILTS := false
WITH_DEXPREOPT := true

# OpenGapps (harmless if not using them) (See https://github.com/opengapps/opengapps/wiki/Package-Comparison for variant types)
GAPPS_VARIANT := pico

# Platform
TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true
TARGET_NO_RECOVERY := true
TARGET_BOARD_PLATFORM := bcm2711

# Kernel
TARGET_NO_KERNEL := false
TARGET_KERNEL_USE_CLANG := true

TARGET_KERNEL_ARCH = $(TARGET_ARCH)
TARGET_KERNEL_CONFIG := zefie_rpi4_defconfig
TARGET_KERNEL_SOURCE := kernel/rpi
#TARGET_KERNEL_HAVE_EXFAT := true
#TARGET_KERNEL_HAVE_NTFS := true
BOARD_KERNEL_IMAGE_NAME := Image
BOARD_KERNEL_BASE := 0x80000000
BOARD_KERNEL_PAGESIZE := 4096

# we wont use this anyway (boot.img), at least not for a while
BOARD_KERNEL_CMDLINE := dwc_otg.lpm_enable=0 console=serial0,115200 earlyprintk no_console_suspend root=/dev/ram0 elevator=noop spidev.bufsiz=65536 rootwait androidboot.hardware=rpi4 androidboot.selinux=permissive
BOARD_MKBOOTIMG_ARGS := --kernel_offset 0x00008000 --ramdisk_offset 0x01f00000 --tags_offset 0x00000100

# Audio
USE_XML_AUDIO_POLICY_CONF := 1

# Bluetooth
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := $(LOCAL_DEVICE_PATH)/bluetooth
BOARD_CUSTOM_BT_CONFIG := $(LOCAL_DEVICE_PATH)/bluetooth/vnd_rpi4.txt
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_BCM := true

# Memory
MALLOC_SVELTE := true

# Partition sizes
BOARD_FLASH_BLOCK_SIZE := 4096
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1610612736 # 1536M
BOARD_USERDATAIMAGE_PARTITION_SIZE := 134217728 # 128M
BOARD_VENDORIMAGE_PARTITION_SIZE := 134217728 # 128M
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_USES_VENDORIMAGE := true
TARGET_COPY_OUT_VENDOR := vendor
#BOARD_HAS_LARGE_FILESYSTEM := true
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true
TARGET_USERIMAGES_USE_EXT4 := true
#TARGET_USERIMAGES_USE_F2FS := true

# SELinux
BOARD_SEPOLICY_DIRS += $(LOCAL_DEVICE_PATH)/sepolicy

# Wifi
BOARD_WLAN_DEVICE := bcmdhd
BOARD_HOSTAPD_DRIVER := NL80211
BOARD_HOSTAPD_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)
WPA_SUPPLICANT_VERSION := VER_0_8_X

# Misc
BOARD_HAS_GPS_HARDWARE := false

