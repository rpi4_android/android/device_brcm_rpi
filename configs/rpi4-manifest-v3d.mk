ifeq ($(TARGET_ARCH), arm)
   # 32-bit
   PRODUCT_COPY_FILES += $(LOCAL_MANIFESTS)/manifest-rpi4-v3d-32.xml:vendor/manifest.xml
else
   # 64-bit
   ifneq ($(TARGET_2ND_ARCH),)
      # 64-bit Multilib
      PRODUCT_COPY_FILES += $(LOCAL_MANIFESTS)/manifest-rpi4-v3d-64-multilib.xml:vendor/manifest.xml
   else
      # 64-bit only
      PRODUCT_COPY_FILES += $(LOCAL_MANIFESTS)/manifest-rpi4-v3d-64.xml:vendor/manifest.xml
   endif
endif
