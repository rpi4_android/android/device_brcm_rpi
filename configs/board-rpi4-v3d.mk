ifeq ($(TARGET_ARCH), arm)
   # 32-bit
   DEVICE_MANIFEST_FILE := $(LOCAL_MANIFESTS)/manifest-rpi4-v3d-32.xml
else
   # 64-bit
   ifneq ($(TARGET_2ND_ARCH),)
      # 64-bit Multilib
      DEVICE_MANIFEST_FILE := $(LOCAL_MANIFESTS)/manifest-rpi4-v3d-64-multilib.xml
   else
      # 64-bit only
      DEVICE_MANIFEST_FILE := $(LOCAL_MANIFESTS)/manifest-rpi4-v3d-64.xml
   endif
endif

TARGET_USES_HWC2 := true
TARGET_HARDWARE_3D := true
BOARD_USES_MINIGBM := true
BOARD_EGL_CFG := $(LOCAL_CONFIGS)/egl.cfg
USE_OPENGL_RENDERER := true
BOARD_GPU_DRIVERS := vc4 kmsro v3d
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 2

