ifeq ($(TARGET_ARCH), arm)
   # 32-bit
   DEVICE_MANIFEST_FILE := $(LOCAL_MANIFESTS)/manifest-swiftshader-32.xml
else
   # 64-bit
   ifneq ($(TARGET_2ND_ARCH),)
      # 64-bit Multilib
      DEVICE_MANIFEST_FILE := $(LOCAL_MANIFESTS)/manifest-swiftshader-64-multilib.xml
   else
      # 64-bit only
      DEVICE_MANIFEST_FILE := $(LOCAL_MANIFESTS)/manifest-swiftshader-64.xml
   endif
endif

TARGET_USES_HWC2 := true
USE_OPENGL_RENDERER := true
