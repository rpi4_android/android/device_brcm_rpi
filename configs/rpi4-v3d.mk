#PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=131072 # 2.0
#PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=196608 # 3.0
PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=196609 # 3.1

PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=213 \
    ro.hardware.hwcomposer=drm \
    ro.hardware.gralloc=minigbm_v3d \
    hwc.drm.device=/dev/dri/card%

#
# 3D Acceleration
#
PRODUCT_PACKAGES += \
    libdrm \
    libsync \
    libstagefrighthw \
    libGLES_android \
    libGLES_mesa \
    vndk-sp \
    android.hardware.renderscript@1.0

#
# Hardware Composer HAL
#
PRODUCT_PACKAGES += \
    hwcomposer.drm \
    hwcomposer.drm_minigbm \
    android.hardware.graphics.composer@2.1-impl \
    android.hardware.graphics.composer@2.1-service

#
# Gralloc HAL
#
PRODUCT_PACKAGES += \
    gralloc.minigbm_v3d \
    android.hardware.graphics.mapper@2.0-impl \
    android.hardware.graphics.allocator@2.0-impl \
    android.hardware.graphics.allocator@2.0-service


PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml
