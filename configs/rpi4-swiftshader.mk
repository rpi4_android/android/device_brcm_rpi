#PRODUCT_PROPERTY_OVERRIDES += ro.sf.lcd_density=260
PRODUCT_PROPERTY_OVERRIDES += ro.sf.lcd_density=213

#PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=131072 # 2.0
PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=196608 # 3.0
#PRODUCT_PROPERTY_OVERRIDES += ro.opengles.version=196609 # 3.1

# Graphics
PRODUCT_PACKAGES += \
    android.hardware.graphics.allocator@3.0-impl \
    android.hardware.graphics.allocator@3.0-service \
    android.hardware.graphics.composer@2.1-impl \
    android.hardware.graphics.composer@2.1-service \
    android.hardware.graphics.mapper@3.0-impl \
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    libEGL_swiftshader \
    libGLESv1_CM_swiftshader \
    libGLESv2_swiftshader \
    libyuv
