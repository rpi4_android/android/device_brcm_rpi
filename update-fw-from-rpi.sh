#!/bin/bash
Z_PWD="$(realpath "$(dirname "${0}")")"
Z_DEST="${PWD}/prebuilt/boot"
Z_FILELIST=(LICENCE.broadcom fixup4.dat fixup4x.dat start4.elf start4x.elf)
Z_BRANCH=stable

if [ ! -z "${1}" ]; then
	Z_BRANCH="${1}"
fi

for f in "${Z_FILELIST[@]}"; do
	z_file=$(basename "${f}")
	z_outfile="${Z_DEST}/${f}"
	echo "Updating ${z_outfile} from rpi ${Z_BRANCH}..."
	curl -sL "https://github.com/raspberrypi/firmware/raw/${Z_BRANCH}/boot/${z_file}" -o "${z_outfile}"
done
