# Kernel
arm_64bit=1
kernel=kernel
initramfs ramdisk.img followkernel

# Audio
dtparam=audio=on

# Display
hdmi_force_hotplug=1
hdmi_drive=2
hdmi_group=1
hdmi_mode=4
disable_overscan=1

# Graphics acceleration
dtoverlay=vc4-fkms-v3d,cma-128
gpu_mem=128
max_framebuffers=2
#dtoverlay=vc4-kms-v3d,cma-256
#mask_gpu_interrupt0=0x400
#avoid_warnings=2

# I2C
dtparam=i2c_arm=on

# Serial console
enable_uart=1
dtdebug=1

# SPI
dtparam=spi=on

