include device/brcm/rpi/configs/local_path.mk

ifeq ($(TARGET_PRODUCT), rpi4)
   # 64-bit multilib
   include $(LOCAL_CONFIGS)/board-rpi4-common.mk
   include $(LOCAL_CONFIGS)/board-rpi4-64bit-multilib.mk
   include $(LOCAL_CONFIGS)/board-rpi4-v3d.mk
else
   include $(LOCAL_DEVICE_PATH)/$(TARGET_PRODUCT)/BoardConfig.mk
endif
