PRODUCT_MAKEFILES := \
        $(LOCAL_DIR)/rpi4/rpi4.mk \
        $(LOCAL_DIR)/rpi4_32/rpi4_32.mk \
        $(LOCAL_DIR)/rpi4_64/rpi4_64.mk \
        $(LOCAL_DIR)/rpi4_swiftshader/rpi4_swiftshader.mk \
        $(LOCAL_DIR)/rpi4_32_swiftshader/rpi4_32_swiftshader.mk

# rpi4 (64-bit multilib w/ MESA)
COMMON_LUNCH_CHOICES := \
        rpi4-eng \
        rpi4-userdebug \
        rpi4-user

# rpi4_32 (32-bit only)
COMMON_LUNCH_CHOICES += \
        rpi4_32-eng \
        rpi4_32-userdebug \
        rpi4_32-user

# rpi4_64 (64-bit only)
COMMON_LUNCH_CHOICES += \
        rpi4_64-eng \
        rpi4_64-userdebug \
        rpi4_64-user

# rpi4_swiftshader (64-bit multilib w/ SwiftShader)
COMMON_LUNCH_CHOICES += \
        rpi4_swiftshader-eng \
        rpi4_swiftshader-userdebug \
        rpi4_swiftshader-user

# rpi4_swiftshader_32 (32-bit w/ SwiftShader)
COMMON_LUNCH_CHOICES += \
        rpi4_32_swiftshader-eng \
        rpi4_32_swiftshader-userdebug \
        rpi4_32_swiftshader-user

