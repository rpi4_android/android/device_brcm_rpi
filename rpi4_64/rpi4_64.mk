include $(LOCAL_PATH)/../configs/local_path.mk

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit_only.mk)

TARGET_USES_V3D := true
include $(LOCAL_CONFIGS)/rpi4-common.mk
include $(LOCAL_CONFIGS)/rpi4-product.mk

PRODUCT_NAME := rpi4_64
